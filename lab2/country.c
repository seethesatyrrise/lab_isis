#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "map.h"

int main(int argc, char * argv[])
{

    COUNTRY ** list;
    char *cmd = (char*)malloc(20);

    list = map_load();        /* Загрузка списка */
    
    printf("\n\nДля вызова помощи введите знак вопроса (?) и нажмите Enter");
    printf("\n");

    while (1) {

        scanf("%s", cmd);
        
        if(strcmp(cmd, "?") == 0) { 
            menu();
        }
        else if (strcmp(cmd, "add") == 0) { 

            char* name = (char*)malloc(255);
            int population = 0;
            int area = 0;

            scanf("%s", name);
            scanf("%d", &population);
            scanf("%d", &area);

            undecorate_name(&name);
            printf("\nCOMMAND:: %s %s %d %d\n", cmd, name, population, area);

            map_add(list, name, population, area);
        }
        else if (strcmp(cmd, "dump") == 0) {
            printf("\nCOMMAND:: %s\n", cmd);
            print();
            map_dump(list);
        }
        else if (strcmp(cmd, "delete") == 0) {
            char* name = (char*)malloc(255);

            scanf("%s", name);
            undecorate_name(&name);
            printf("\nCOMMAND:: %s %s\n", cmd, name);

            map_delete(list, name); /* находим страну и удаляем */
            
        }
        else if(strcmp(cmd, "view") == 0) {
            char* name = (char*)malloc(255);

            scanf("%s", name);
            undecorate_name(&name);

            printf("\nCOMMAND:: %s %s\n", cmd, name);
            
            COUNTRY *country_to_view = map_find(list, name);
            if (country_to_view){
                print();
                printf("  %.2d\t|", hash(key_gen(name)));
                print_country(country_to_view);
            }
            else
                print_error(2);    
        }
        else if(strcmp(cmd, "save") == 0){
            map_save(list);
            printf("\nSAVED SUCCESSFULLY\n");
        }
        else if(strcmp(cmd, "find") == 0){
            char* name = (char*)malloc(255);
            scanf("%s", name);

            printf("\nCOMMAND:: %s %s\n", cmd, name);
            
            COUNTRY *to_find = map_find(list, name);
            
            if (to_find != NULL){
                print();
                printf("  %.2d\t|", hash(key_gen(name)));
                print_country(to_find);
            }
            else
                print_error(2);

        }
        else if (strcmp(cmd, "count") == 0){
            printf("\nКоличество записей = %d\n\n", map_count(list));
        }
        else if (strcmp(cmd, "quit") == 0){
            break;
        }
        else{
            print_error(0);
            menu();
        }
    }
    
    /* Удаление списка из динамической памяти */
    map_clear(list);
    printf("\nКоличество записей = %d\n\n", map_count(list));

    return 0;
}
