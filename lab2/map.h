#ifndef _MAP_H_
#define _MAP_H_
#include "list.h"

COUNTRY ** map_create(void);
void map_clear(COUNTRY ** map);
void map_add( COUNTRY ** map, char * name, int population, int area);
void map_delete(COUNTRY ** map, char* name);
COUNTRY * map_find(COUNTRY ** map, char * name);
void map_dump(COUNTRY ** map);
COUNTRY ** map_load();
void map_save(COUNTRY ** map);
void print();
int map_count(COUNTRY ** map);

void menu();
void print_error(short error_type);
void undecorate_name(char ** name);

#endif // _MAP_H_

