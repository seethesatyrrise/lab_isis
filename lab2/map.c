#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "map.h"

#define DBNAME "./DATABASE.csv"
#define MAP_SIZE 64
#define MAX_LINE_LENGTH 255

/* 
 * Возвращает значение хэша ключа. Ключ это название страны.
 */
unsigned int hash(int key)
{
    unsigned int S = 3*5*7*11; 
    return (S * key) % MAP_SIZE;
}

/*
 * Генерирует ключ посредством суммирования всех букв в строке имени страны
 * для предачи в функцию генерации хэша
*/
unsigned int key_gen(char *name)
{
    int i, sum = 0;

    for (i = 0; name[i]; i++)
        sum += name[i];

    return sum;
}

/* Добавляет элемент к хэш таблице */
//void map_add( COUNTRY ** map, char * name, int population, int area)
//{
//    int index = hash(key_gen(name));
//    add(&(map[index]), name, population, area);
//}

void map_add( COUNTRY ** map, char * name, int population, int area)
{
     int index = hash(key_gen(name));
    COUNTRY * p = find(map[index], name);
    if (p == NULL)
        add(&map[index], name, population, area);
    else
    {
        p->population = population;
        p->area = area;
    }   
}

/* Удаляет страну с указанным названием из хеш таблицы */
void map_delete(COUNTRY ** map, char* name)
{
    COUNTRY *to_delete = map_find(map, name);
    if (to_delete != NULL){
        int index = hash(key_gen(name));
        print();
        printf("  %.2d\t|", index);
        print_country(to_delete);
        delete(&(map[index]), to_delete);
    }else{
        print_error(1);
    }
}

/* Ищет страну с указанным названием в хеш таблице */
COUNTRY * map_find(COUNTRY ** map, char * name)
{
    int index = hash(key_gen(name));
    return find(map[index], name);
}

/* Печатает на экране все записи хеш таблицы */
void map_dump(COUNTRY ** map)
{
    COUNTRY* node;
    int index = 0;
    while(index != MAP_SIZE){
        for(node = map[index]; node != NULL; node = node->next){
        //dump(map[index]);
            printf("  %.2d\t|", index);
            print_country(node);
        }
        index++;
    }
}
/*функция счета элементов в хэш таблице*/
int map_count(COUNTRY ** map){
    int cnt = 0, index = 0;
    while(index++ != MAP_SIZE){
        cnt += count(map[index]);
    }

    return cnt;
}

/* Создает хэш таблицу */
COUNTRY ** map_create(void)
{
    COUNTRY ** map = (COUNTRY **)malloc(sizeof(COUNTRY *) * MAP_SIZE);
    memset(map, 0, sizeof(COUNTRY *) * MAP_SIZE);
    return map;
}

/* Удаляет хэш таблицу */
void map_clear(COUNTRY ** map)
{
    int cnt;
    for (cnt = 0; cnt < MAP_SIZE; cnt++){
        if (map[cnt] != NULL){
            clear(&(map[cnt]));
        }
        free(map[cnt]);
    }
}


/* Загружает таблицу из файла */
COUNTRY ** map_load()
{
    char buf[MAX_LINE_LENGTH + 1];
    char * par[3];
    int cnt, pcount = 0;
    COUNTRY *p, ** map = NULL;
    FILE * f = fopen(DBNAME, "r");

    map = map_create();

    buf[MAX_LINE_LENGTH] = 0x00; 
 
    if (f) {
        while(fgets(buf, MAX_LINE_LENGTH, f)) {
            pcount = 0;
            par[pcount++] = buf;
            cnt = 0;
            while(buf[cnt]) {
                if (buf[cnt] == ',') {
                    buf[cnt] = 0x00;
                    par[pcount++] = &buf[cnt + 1];
                }
                cnt++;
            }
            if (pcount == 3) {
                map_add(map, par[0], atoi(par[1]), atoi(par[2])); 
            }           
        }
        fclose(f);
    }
    return map;
}

/* Сохраняет таблицу в файл */
void map_save(COUNTRY ** map)
{
    COUNTRY * p;
    int cnt;
    FILE * f = fopen(DBNAME, "w+");

    if (f) {
        for (cnt = 0; cnt < MAP_SIZE; cnt++) {
            p = map[cnt];
            while (p != NULL) {
                fprintf(f, "%s,%d,%d\n", p->name, p->population, p->area);
                p = p->next;
            }
        }
        fclose(f);
    }
}

void menu(){
    printf("\nКоманды консоли выглядят следующим образом:\n\n");
    printf("add <СТРАНА> <НАСЕЛЕНИЕ> <ПЛ.СТРАНЫ>\tдобавить новую страну\n");
    printf("dump \tвывод Базы Данных\n");
    printf("count\tвывод количества записей в базе данных\n");
    printf("delete <СТРАНА>\tудаление страны <СТРАНА>\n");
    printf("view <СТРАНА>\tотображение данных по стране <СТРАНА>\n\n");
}

void print_error(short error_type){
    switch(error_type){
        case 0:{/* недостаточно аргументов */
            printf("-----НЕ КОРРЕКТНО ВВЕДЕНА КОМАНДА-----\n\n");
            break;
        }
        case 1:{
            printf("\n-----УДАЛЯЕМАЯ СТРАНА НЕ НАЙДЕНА-----\n\n");
            break;
        } 
        case 2:{
            printf("\n-----СТРАНА НЕ НАЙДЕНА-----\n\n");
            break;
        }

    }
}

void undecorate_name(char ** name)
{
    int cnt = 0;
    while ((*name)[cnt]) {
        if ((*name)[cnt] == '_')
            (*name)[cnt] = ' ';
        cnt++;
    }
}

void print(){
    printf("  %s |", "INDEX");
    printf("\t%s\t|\t%s\t|\t%s\n", "NAME", "POPULATION", "AREA");
    printf("\n");
}
