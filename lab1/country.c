#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"

int main(int argc, char * argv[])
{
    COUNTRY * list;
    if (argc < 2)
    {
        printf("%s\n", "Запустите программу с указанием одной из команд:");
        printf("%s\n", "add <название> <население> <площадь>");
        printf("%s\n", "delete <название страны>");
        printf("%s\n", "dump <необязательные параметры сортировки -n, -a, -p>");
        printf("%s\n", "view <название страны>");
        printf("%s\n", "count");
        printf("********%s%s********\n", "Файл с данными должен иметь имя ", FILE_NAME);
        return 1;
    }
    else
    {
        /* Загрузка списка  из файла  */
        list = load();

        /* Проверка и выполнение команд */

        /* Функция добавления в базу данных */
        if (!strcmp(argv[1], "add") && argc == 5)
        {
            add(&list, argv[2], atoi(argv[3]), atoi(argv[4]));
            save(list);
        }
        /* Функция удаления из базы данных*/
        else if (!strcmp(argv[1], "delete") && argc == 3)
        {
            COUNTRY * p = find(list, argv[2]);
            if(p == NULL)
                printf("Элемента с именем %s не обнаружено\n", argv[2]);
            else
            {
                delete(&list, p); 
                save(list);
            }     
        }

        /* Функция вывода на экран всех элементов базы данных с необязательными параметрами*/
        else if (!strcmp(argv[1], "dump"))
        {
            if (argc == 3)
            {
                if (!strcmp(argv[2], "-n"))
                {
                    sort_by_name(&list);
                    dump(list);
                }
                else if(!strcmp(argv[2], "-p"))
                {
                    sort_by_population(&list);
                    dump(list);               
                }
                else if(!strcmp(argv[2], "-a"))
                {
                    sort_by_area(&list);
                    dump(list);               
                }  
                else if(1)
                {
                    printf("%s\n", "Аргументы введены неправильно!");
                    return 1;
                }

            }
            else if(argc == 2)
                dump(list);
            else if(1)
            {
                printf("%s\n", "Аргументы введены неправильно!");
                return 1;
            }
        }
        /* Функция отображения сведений о элементе*/
        else if (!strcmp(argv[1], "view") && argc == 3)
        {
            COUNTRY * p = find(list, argv[2]);
            if(p == NULL)
                printf("Элемента с именем %s не обнаружено\n", argv[2]);
            else
                print_country(p);
        }

            
        /* Функция вывода количества элементов базы данных*/
        else if (!strcmp(argv[1], "count") && argc == 2)       
            printf("%i\n", count(list));

        else if(1)
        {
            printf("%s\n", "Аргументы введены неправильно!");
            return 1;
        }
        
        /* Удаление списка из динамической памяти */
        clear(&list);
        return 0;
            
    }
}
