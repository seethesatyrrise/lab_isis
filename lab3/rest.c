
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#define host_name "www.iu3.bmstu.ru"
#define portno 80
#define println printf("\n")

struct sockaddr_in ad, sa, ca;
struct hostent * ht;
int sc;

int init_adress(){
    /*
        Структура адреса
    */
    //int sc;
    sc = socket(AF_INET, SOCK_STREAM, 0); /* IPv4 */
    if (sc < 0) {
        printf("cant open socket %d\n", sc);
    }

    ht = gethostbyname(host_name);
    if (ht == NULL) {
        printf("NO SUCH HOST\n");
        exit(0);
    }

    /* Подготовим структуру заполнив ее нулями */
    memset(&ad, 0, sizeof(ad));

    ad.sin_family = AF_INET;

    /* Копируем первый адрес из структуры hostent */
    memcpy(&ad.sin_addr.s_addr, ht->h_addr, ht->h_length);

    /* Номер порта сервера */
    ad.sin_port = htons(portno);

    /*  Пытаемся подключиться   */
    if (connect(sc, (struct sockaddr *)&ad,sizeof(ad)) < 0) {
        printf("\t::NOT CONNECTED::connect() < 0\n");
        return 0;
    }
    else{
        printf("\t::CONNECTED SUCCESSFULLY\n\n");
        return 1;
    }
}
    /*
        Делаем запрос
    */

int make_request(char *request){

    int n, len;
    char resp[256];
    
    len = strlen(request);
    n = 0;
    while (len > 0) 
    {
        n = write(sc, &request[n], len);
        if (n < 0)
            break;
        len -= n;
    }
    if (n < 0) {
        printf("::ERROR\n"); 
        exit(0);
    }
    char flag = '1';
    while (1) {
        n = read(sc, resp, 255);
        if (n > 0) {
            if (strstr(request, "GET") == NULL && flag){
                if (strstr(resp, "HTTP/1.1 200 OK") == NULL){
                    printf("::INVALID TYPE OF REQUEST::NO HTTP/1.1 200 OK\n");
                    println;
                    return 0;
                }
                flag = '\0';
            }
            resp[n] = 0;
            printf(resp);
            printf("\n");
        }
        else
            break;
    }
    if (n < 0) { 
        printf("::ERROR\n"); 
        exit(0);
    }

    close(sc);
    return 0;
}

void menu(){
    println;
    printf("Для ввода команды используйте следующий формат (\\ - ИЛИ):\n");
    printf("-get\\post -t utc\\local -f unix\\internet\n");
    printf("-t utc - время UTC\n-t local - локальное время сервера\n");
    printf("-f unix - время UNIX\n-f internet - время RFC3339\n");
    printf("-post - POST запрос. \nПо умолчанию GET запрос\n");
    println;
}

char* command_line_parser(int args_count, char *args[]){
    char *command_line_str = (char*)malloc(40);
    char *request = (char*)malloc(1024);
    int i;
    if (args_count < 2)
        return NULL;
    if (strcmp(args[1], "-g") == 0)
        i = 2;
    else
        i = 1;
    if (args_count > 2){
        strcpy(command_line_str, args[i]);
        i++;
        for (; i < args_count; i++)
        {
            strcat(command_line_str, " ");
            strcat(command_line_str, args[i]);
        }
        i = 0;

        printf("command_line_str = %s\n",  command_line_str);
        if (strstr(command_line_str, "-t") != NULL){
            if (strstr(command_line_str, "utc") != NULL){
                printf("UTC\n");
                strcat(request, "type=utc");
            }
            else if (strstr(command_line_str, "local") != NULL){
                strcat(request, "type=local");
            }
        }
        if (strstr(command_line_str, "-f") != NULL){
            if (strstr(request, "type") != NULL){
                if (strstr(command_line_str, "unix") != NULL){
                    strcat(request, "&format=unix\n");
                }
                else if (strstr(command_line_str, "internet") != NULL){
                    strcat(request, "&format=internet\n");
                }
            }
            else{
                strcat(request, "type=local");
                printf("request = %s\n", request);
                if (strstr(command_line_str, "unix") != NULL){
                    strcat(request, "&format=unix\n");
                }
                else if (strstr(command_line_str, "internet") != NULL){
                    strcat(request, "&format=internet\n");
                }
            }
        }
        if (strstr(command_line_str, "-f") == NULL){
            strcat(request, "&format=internet\n");
        }
    }
    else
        return NULL;
    printf("request parsed= %s\n", request);

    return request; 
}

#define GET "GET /WebApi/time?"
#define DEFAULT "GET /WebApi/time?type=local&format=internet\n"
#define POST "POST /WebApi/time HTTP/1.0"
#define CONTENT_TYPE "\nContent-Type:application/x-www-form-urlencoded"
#define CONTENT_LEN "\nContent-Length:20\n\n"

int main(int argc, char *argv[]){
    if (argc < 5){
        printf("\n::ERROR::NOT ENOUGH FLAGS\n\n");
        menu();
        //return 0;
    }
    char *request = (char*)malloc(1024);
    char* request_from_cline = command_line_parser(argc, argv);
    if (request_from_cline == NULL){
        strcpy(request, DEFAULT);
    }
    else{
        printf("NOT_NULL\n");
        if (strcmp(argv[1], "-p") == 0){
            strcpy(request, POST);
            strcat(request, CONTENT_TYPE);
            strcat(request, CONTENT_LEN);
            strcat(request, request_from_cline);
        }
        else{
            strcpy(request, GET);
            strcat(request, request_from_cline);
        }
        
    }
    printf("to server request = %s\n", request);
    init_adress();
    make_request(request);

    return 0;
}
