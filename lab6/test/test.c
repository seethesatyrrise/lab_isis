#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include "../hello.h"

#define file_name "/dev/hello"

int f;

int reset(void)
{
    int err = ioctl(f, LEDCMD_RESET_STATE);
    if (err != 0)
    {
        printf("reset: error\n");
    }
    return err;
}

int ledstate1(unsigned char pin)
{
    struct led_t led;
    led.pin = pin;
    int err = ioctl(f, LEDCMD_GET_LED_STATE, &led);
    if (err != 0)
    {
        printf("ledstate: error\n");
    }
    else
    {
        printf("%d\n", led.value);
    }
    return err;
}

int ledstate0(void)
{
    unsigned char state;
    int err = ioctl(f, LEDCMD_GET_STATE, &state);
    if (err != 0)
    {
        printf("ledstate: error\n");
    }
    else
    {
        for (int i = 0; i < 8; ++i)
        {
            unsigned char value = (state & (128 >> i)) != 0;
            printf("%d", value);
        }
        printf("\n");
    }
    return err;
}

int setstate(unsigned char pin, unsigned char state)
{
    struct led_t led;
    led.pin = pin;
    led.value = state;
    int err = ioctl(f, LEDCMD_SET_LED_STATE, &led);
    if (err != 0)
    {
        printf("setstate: error\n");
    }
    return err;
}

int main(int argc, char * argv[])
{
    f = open(file_name, O_RDWR); /* получаем ссылку на устройство */
    if (f == -1)
    {
        printf("device opening error\n");
        return -1;
    }

    int err = 0;

    if (argc == 1)
    {
        printf("format: test <command> <args>\n");
        err = 1;
    }

    char* cmd = argv[1];
    if (!strcmp(cmd, "reset"))
    {
        err = reset();
    }
    else if (!strcmp(cmd, "ledstate"))
    {
        if (argc == 2)
        {
            err = ledstate0();
        }
        else
        {
            err = ledstate1(atoi(argv[2]));
        }
    }
    else if (!strcmp(cmd, "on"))
    {
        if (argc < 3)
        {
            printf("too few arguments\n");
            err = 1;
        }
        else
        {
            err = setstate(atoi(argv[2]), 1);
        }
    }
    else if (!strcmp(cmd, "off"))
    {
        if (argc < 3)
        {
            printf("too few arguments\n");
            err = 1;
        }
        else
        {
            err = setstate(atoi(argv[2]), 0);
        }
    }
    else
    {
        printf("command is not recognized\n");
        err = 1;
    }

    return err;
}
