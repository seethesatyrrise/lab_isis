/* hello.c – Заготовка для второй лабораторной работы */
#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/string.h> 
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/skbuff.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/udp.h>
#include <linux/in.h>

static dev_t dev;
static struct cdev c_dev;
static struct class * cl;

static int my_open(struct inode *inode, struct file *file);
static int my_close(struct inode *inode, struct file *file);
static ssize_t my_read(struct file *filp, char *buffer, size_t length, loff_t * offset);
static ssize_t my_write(struct file *filp, const char *buff, size_t len, loff_t * off);        

static struct file_operations hello_fops =
{
	.owner = THIS_MODULE,
	.open = my_open,
	.release = my_close,
	.read = my_read,
	.write = my_write
};

static int __init hello_init(void) /* Инициализация */
{
	int retval;
	bool allocated = false;
	bool created = false;
	cl = NULL;

	retval = alloc_chrdev_region(&dev, 0, 1, "hello");
	if (retval)
		goto err;

	allocated = true;
	printk(KERN_INFO "Major number = %d Minor number = %d\n", MAJOR(dev), MINOR(dev));
	cl = class_create(THIS_MODULE, "teach_devices");
	if (!cl) {
		retval = -1;
		goto err;
	}

	if (device_create(cl, NULL, dev, NULL, "hello") == NULL)
	{
		retval = -1;
		goto err;
	}

	created = true;
	cdev_init(&c_dev, &hello_fops);
	retval = cdev_add(&c_dev, dev, 1);
	if (retval)
		goto err;

	printk(KERN_INFO "Hello: regisered");
	return 0;

err:
	printk("Hello: initialization failed with code %08x\n", retval);
	if (created)
		device_destroy(cl, dev);

	if (allocated)
		unregister_chrdev_region(dev, 1);

	if (cl)
		class_destroy(cl);

	return retval;
}

#define BUF_SIZE 1024
#define BUF_MASK BUF_SIZE - 1
char round_buf[BUF_SIZE];
int read = 0;
int write = 0;

void BufWrite(const char chr)
{
	round_buf[write++ & BUF_MASK] = chr;
}

const char BufRead(void)
{
	return round_buf[read++ & BUF_MASK];
}

/* Структура, которая используется для регистрации обработчика. */
static struct nf_hook_ops nfho;

void WriteAddr(__be32* addr)
{
	unsigned char* i = (unsigned char*)addr;
	for (; i < (unsigned char*)(addr + 1); ++i)
	{
		printk(KERN_INFO "WriteAddr %d\n", *i);
		
		char num[4];
		sprintf(num, "%d", *i);

		char* j = num;
		for (; *j != '\0'; ++j)
		{
			BufWrite(*j);
		}

		BufWrite('.');
	}
}

void WritePort(__be16 port)
{
	printk(KERN_INFO "WritePort %d\n", port);

	char num[10];
	sprintf(num, "%d", port);

	char* i = num;
	for (; *i != '\0'; ++i)
	{
		BufWrite(*i);
	}
}

void WriteProtocol(__u8 protocol)
{
	printk(KERN_INFO "WriteProtocol %d\n", protocol);

	if (protocol == IPPROTO_TCP)
	{
		BufWrite('T');
		BufWrite('C');
		BufWrite('P');
	}
	else if (protocol == IPPROTO_UDP)
	{
		BufWrite('U');
		BufWrite('D');
		BufWrite('P');
	}
}

/* Собственно функция обработчика. */
unsigned int hook_func(void *ops,
			       struct sk_buff *skb,
			       const struct nf_hook_state *state)
{
    struct iphdr* ip = ip_hdr(skb);

	WriteProtocol(ip->protocol);
	BufWrite(' ');

	WriteAddr(&ip->saddr);
	BufWrite(':');

	__be16* hdr = (__be16*)(skb->data + (ip->ihl * 4));
	
	WritePort(*hdr);
	BufWrite('>');

	WriteAddr(&ip->daddr);
	BufWrite(':');

	WritePort(*(++hdr));
	BufWrite('\n');

	return NF_ACCEPT;
}

/* Процедура регистрирует обработчик сетевых пакетов */
int ip_handler_register(void)
{
	/* Fill in our hook structure */
	nfho.hook     = hook_func;
	/* Handler function */
	nfho.hooknum  = NF_INET_PRE_ROUTING; /* Первое сетевое событие для IPv4 */
	nfho.pf       = PF_INET;
	nfho.priority = NF_IP_PRI_FIRST;   /* Назначаем обработчику 1-ый приоритет */

	struct net* n;
	for_each_net(n)
        nf_register_net_hook(n, &nfho);
	printk(KERN_INFO "Hello: Netfilter hook registered!\n");
	return 0;
}

/* Процедура отменяет регистрацию обработчика сетевых пакетов */
void ip_handler_unregister(void)
{
	struct net* n;
	nf_unregister_net_hook(n, &nfho);
	printk(KERN_INFO "Hello: Netfilter hook unregistered!\n");
}

static int my_open(struct inode *inode, struct file *file)
{
	printk(KERN_INFO "Hello: open\n");
	ip_handler_register();
	return 0;
}

static int my_close(struct inode *inode, struct file *file)
{
	printk(KERN_INFO "Hello: close\n");
	ip_handler_unregister();
	return 0;
}

static ssize_t my_read(struct file *filp,  
                           char *buffer, /* буфер данных */
                           size_t length, /* длина буфера */
                           loff_t * offset)
{
	char chr = '\0';

	if (read != write)
	{
		chr = BufRead();	
	}

	if (copy_to_user(buffer, &chr, 1))
	{
		return -EFAULT;
	}

	return 1;
}

static ssize_t my_write(struct file *filp, const char *buff, size_t len, loff_t * off)
{
        return -EINVAL;
}


static void __exit hello_exit(void) /* Деинициализаия */
{
    printk(KERN_INFO "Hello: unregistered\n");
    device_destroy (cl, dev);
    unregister_chrdev_region (dev, 1);
    class_destroy (cl);
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ivan Sidyakin");
MODULE_DESCRIPTION("Simple loadable kernel module");
